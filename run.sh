#!/bin/bash

set -e
set -o pipefail

M2_HOME=dist/maven
ANT_HOME=dist/ant
url=http://teamcity.jetbrains.com/guestAuth/repository/download/bt343/lastSuccessful

rm -rf   download
mkdir -p download/http download/ivy download/gradle download/maven


echo -=-=-=-=-=-=-=-=-= HTTP: listing IDEA artifacts =-=-=-=-=-=-=-=-=-

curl --silent $url/teamcity-ivy.xml

echo -=-=-=-=-=-=-=-=-= HTTP: listing IDEA build file =-=-=-=-=-=-=-=-=-

curl --silent $url/sources.zip!build.xml

echo -=-=-=-=-=-=-=-=-= HTTP: downloading IDEA annotations =-=-=-=-=-=-=-=-=-

wget -nv      $url/core/annotations.jar -O download/http/annotations.jar

echo -=-=-=-=-=-=-=-=-= Ant + Ivy: downloading IDEA annotations =-=-=-=-=-=-=-=-=-

$ANT_HOME/bin/ant -f build.xml

echo -=-=-=-=-=-=-=-=-= Gradle: compiling sources =-=-=-=-=-=-=-=-=-

./gradlew clean build copyJar

echo -=-=-=-=-=-=-=-=-= Maven: compiling sources =-=-=-=-=-=-=-=-=-

$M2_HOME/bin/mvn clean compile


echo  "* download/http      : `ls download/http`"
echo  "* download/ivy/core  : `ls download/ivy/core`"
echo  "* download/gradle    : `ls download/gradle`"
echo  "* download/maven     : `ls download/maven`"
echo  "* build/classes/main : `ls build/classes/main`"
echo  "* target/classes     : `ls target/classes`"
